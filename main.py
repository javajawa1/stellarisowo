#!/usr/bin/python3
# -*- coding: utf-8 -*-
# vim: nospell ts=4 expandtab

from __future__ import annotations

import os
import shutil

import owo


LANG = "english"
INSTALL = "/media/falcon/steam/steamapps/common/Stellaris"
OUTPUT = "/home/benedict/.local/share/Paradox Interactive/Stellaris/mod"
MOD_NAME = "owo_mod"
VERSION = "2.7.*"


def main() -> None:
    local_dir = os.path.join(INSTALL, "localisation")
    out_dir = os.path.join(OUTPUT, "owo_mod", "localisation", "replace")

    os.makedirs(out_dir, exist_ok=True)

    with open(os.path.join(OUTPUT, "owo_mod.mod"), "w", encoding="utf-8") as mod_file:
        mod_file.write("\ufeff")
        mod_file.write(f'name="OwO Localisation Mod ({LANG})"\n')
        mod_file.write(f'path="mod/{MOD_NAME}"\n')
        mod_file.write(f'supported_version="{VERSION}"\n')
        mod_file.write("dependencies={\n}\n")
        mod_file.write('tags={\n\t"Localisation"\n}\n')

    shutil.copy(
        os.path.join(OUTPUT, f"{MOD_NAME}.mod"),
        os.path.join(OUTPUT, MOD_NAME, "descriptor.mod"),
    )

    owo.convert_to_owo(local_dir, out_dir, LANG, LANG)


if __name__ == "__main__":
    main()
