#!/usr/bin/python3
# -*- coding: utf-8 -*-
# vim: nospell ts=4 expandtab

from __future__ import annotations

from typing import List, Optional, Pattern, Tuple


import os
import re


EXPRESSIONS: List[Tuple[Pattern[str], str]] = [
    # Bracket expressions (e.g. "[rRoot.GetName]")
    (re.compile(r"(?P<expression>\[[^\]]+\])", re.MULTILINE), "%_1%"),
    # Grabs the colour codes (e.g. "§B")
    (re.compile(r"(?P<color_code>§[BEGHLMPRSTWY])", re.MULTILINE), "%_2%"),
    # Grabs dollar formatting strings (e.g. "$foo|*0$")
    (re.compile(r"(?P<variable_dollar>\$[^$]+\$)", re.MULTILINE), "%_3%"),
    # Grabs pound custom icons (e.g. "£food£")
    (re.compile(r"(?P<variable_pound>\£[a-z_]+)", re.MULTILINE), "%_4%"),
]


def convert_to_owo(
    source_path: str,
    dest_path: Optional[str] = None,
    source_localisation: str = "english",
    dest_localisation: str = "owo",
) -> None:
    source_dir = os.path.join(source_path, source_localisation)

    if dest_path is None:
        dest_path = source_path

    dest_dir = os.path.join(dest_path, dest_localisation)

    if not os.path.isdir(dest_dir):
        os.mkdir(dest_dir)

    file_list = os.listdir(source_dir)

    for file_to_convert in file_list:
        dest_file_name = file_to_convert.replace(source_localisation, dest_localisation)

        source_file = os.path.join(source_dir, file_to_convert)
        dest_file = os.path.join(dest_dir, dest_file_name)

        print(f"Converting {file_to_convert}…")
        convert_file(source_file, dest_file, dest_localisation)


def convert_file(source_file: str, dest_file: str, dest_localisation: str) -> None:
    with open(dest_file, "w", encoding="utf-8") as dest_handle, open(
        source_file, "r", encoding="utf-8"
    ) as source_handle:
        # Write the Byte Order Mark and the language header
        dest_handle.write("\ufeff")
        dest_handle.write(f"l_{dest_localisation}:\n")

        # Skip the DOM and language header on the source
        source_handle.readline()

        # Process all lines in the file
        for line in source_handle:
            dest_handle.write(convert_line(line) + "\n")


def convert_line(line: str) -> str:
    line = line.strip()

    if line.startswith("#"):
        return line

    if ":" not in line:
        return line

    version = ""
    [key, value] = line.split(":", 1)

    if not (value.startswith(" ") or value.startswith('"')):
        if " " not in value:
            print(line)
            raise Exception
        [version, value] = value.split(" ", 1)

    if not value:
        return line

    value = convert_text(value)

    return f'{key}:{version} "{value}"'


def convert_text(value: str) -> str:
    value = value.strip().strip('"')

    captured: List[List[str]] = []

    # Capture all the magic character groups
    for (capture_regex, replace) in EXPRESSIONS:
        captured.append([replace] + capture_regex.findall(value))

    # If there aren't any, we can just OwO the entire line
    if sum(len(c) - 1 for c in captured) == 0:
        return owo_magic(value)

    # Replace all the magic groups we captured with markers that won't
    # get messed around the OwO-ification.
    for (capture_regex, replace) in EXPRESSIONS:
        value = re.sub(capture_regex, replace, value)

    # OwO the string
    value = owo_magic(value)

    # Put the captured variables back in.
    values: List[str] = []
    for [replace, *values] in captured:
        for expression in values:
            value = re.sub(replace, expression, value, count=1)

    return value


def owo_magic(non_owo_string: str) -> str:
    """
    Converts a non_owo_stirng to an owo_string

    :param non_owo_string: normal string

    :return: owo_string
    """

    return (
        non_owo_string.replace("ove", "wuw")
        .replace("R", "W")
        .replace("r", "w")
        .replace("L", "W")
        .replace("l", "w")
    )
